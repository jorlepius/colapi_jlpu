var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');
var serve = require('../server_v3');

var should =chai.should();

chai.use(chaihttp);

describe('pruebas colapi', () => {
 it('Test Conectividad', (done) => {
   chai.request('http://www.bbva.com')
     .get('/')
     .end((err, res)=>{
      //   console.log(res);
         res.should.have.status(200)
    //     res.header['x-amz-']
         done()
       })

 });
 it('Mi api funciona', (done) => {
   chai.request('http://localhost:3010')
   .get('/colapi/v3/users')
   .end((err, res) => {
     res.should.have.status(200)
     done()
   })
 });
 it('devuelve un array ', (done) => {
   chai.request('http://localhost:3010')
   .get('/colapi/v3/users/')
   .end((err, res) => {
    // res.should.have.status(200)
  //  console.log(res.body)
    res.body.should.be.a('array')
     done()
   })
 });
 it('devuelve por lo menos un elemnto ', (done) => {
   chai.request('http://localhost:3010')
   .get('/colapi/v3/users/')
   .end((err, res) => {
    // res.should.have.status(200)
  //  console.log(res.body)
    res.body.length.should.be.gte(1)
     done()
   })
 });

 it('validar primer elemento ', (done) => {
   chai.request('http://localhost:3010')
   .get('/colapi/v3/users/')
   .end((err, res) => {
    // res.should.have.status(200)
  //  console.log(res.body)
    res.body[0].should.have.property('first_name')
    res.body[0].should.have.property('last_name')
     done()
   })
 });

 it('validar crear elemento ', (done) => {
   chai.request('http://localhost:3010')
   .post('/colapi/v3/users/')
   .send('{"first_name":"Leonardo", "last_name":"Pinto", "email": "leo@bbva.us", "password": "1111111"}')
   .end((err, res, body) => {
    // res.should.have.status(200)
  //  console.log(res.body)
    console.log(body)

     done()
   })
 });

});
