# Imagen inicial apartir de la cual creamos nuestra imagen.
FROM node

#Creando un directorio del contenedor
WORKDIR /colapi_jlpu
#Añadimos contenido del proyecto en directorio de contenedor
ADD . /colapi_jlpu

# Puerto escucha el contenedor
EXPOSE 3010

#Comandos para lanzar nuestra API REST 'colapi'
CMD ["npm", "start"]
#CMD ["node", "server.js"]
