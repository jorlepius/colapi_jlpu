var express = require('express'); /*Import de libreria express*/
var bodyParser = require('body-parser');/*Import de libreria body-parser*/
var requestJson = require('request-json');
var app = express();/*Asignacion de la libreria a una variable*/
var port = process.env.PORT || 3010;
//var usersFile = require('./users.json');
var URLbase = "/colapi/V3/";
var urlBaseMlab = "https://api.mlab.com/api/1/databases/colapidb_jlpu/collections/";
var urlApiKey = "apiKey=vRuHHFuycCYakev5CJelR4O-z6ddCsGp";

app.listen(port);

console.log("Colapi escuchando en puerto "+ port +"...");
app.use(bodyParser.json());//
//CRUD PARA USER
// GET users a traves de Mlab

app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET /colapi/v3/users");
   var httpClient = requestJson.createClient(urlBaseMlab);
   console.log("Cliente HTTP Mlab creado ");
   var fil="f={_id:0}&";
   httpClient.get('user?' + fil + urlApiKey,
    function(err, respuestaMlab, body){
    //  console.log("Error: " + err);
    //  console.log("Body: " + body);
    //  console.log("respuestaMlab: " + respuestaMlab);
      var respuesta = {};
      respuesta = !err ? body : {"msg" : "Error al recuperar usuarios de mlab"}
      res.send(respuesta);

    });
});


// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id',
 function (req, res) {
  // console.log("GET /colapi/v2/users/:id/:otro");
   var httpClient = requestJson.createClient(urlBaseMlab);
  // console.log("Cliente HTTP Mlab creado ");
   var id = req.params.id;
   var fil='f={"_id":0}&q={"id":' + id + '}&';
   httpClient.get('user?' + fil + urlApiKey,
    function(err, respuestaMlab, body){
    //  console.log("Error: " + err);
    //  console.log("Body: " + body);
    //  console.log("respuestaMlab: " + respuestaMlab);
      respuesta = !err ? body : {"msg" : "Error al recuperar usuarios de mlab"};
      if (body[0]==undefined){
        res.send(204);
      }else{

        var respuesta = body[0];
        res.send(respuesta);
      }

    });
});
//POST con mLab
app.post(URLbase + 'users',
function(req, res) {
   var fil="f={_id:0}&";
  var clienteMlab = requestJson.createClient(urlBaseMlab + "user?" + fil + urlApiKey);
  clienteMlab.get('',
    function(err, respuestaMlab, bodyusers){
      var newId=bodyusers.length+1;
      var newJson={};
      newJson=req.body;
      newJson.id=newId;
  //    console.log(newJson);
        clienteMlab = requestJson.createClient(urlBaseMlab + "/user?" + urlApiKey);
        clienteMlab.post('', newJson,
          function(err, resM, body) {
            res.send(body);
          });
        });
      });

app.put(URLbase + 'users/:id',
function(req, res) {
 clienteMlab = requestJson.createClient(urlBaseMlab + "/user")
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
  clienteMlab.put('?q={"id": ' + req.params.id + '}&' + urlApiKey, JSON.parse(cambio),
  function(err, resM, body) {
    respuesta = body.n !=0 ? {"msg" : "Actualizacion realizada"} : {"msg" : "Error usuario no existe"};
      res.send(respuesta);
    });
  });

app.delete(URLbase + 'users/:id',
function (req, res) {
  clienteMlab = requestJson.createClient(urlBaseMlab + "/user")
  var cambio = '{}';
  clienteMlab.put('?q={"id": ' + req.params.id + '}&' + urlApiKey, [],
  function(err, resM, body) {
    //console.log(err);
    //console.log(resM);
    //console.log(resM.statusCode)
    //console.log(resM.statusMessage)
    respuesta = body.removed !=0 ? {"msg" : "Delete realizada"} : {"msg" : "Error usuario no existe"};
    res.send(respuesta);
});
});

//CRUD PARA ACCOUNT

// GET Account a traves de Mlab
app.get(URLbase + 'users/:user_id/accounts/',
 function(req, res) {
   console.log("GET /colapi/v3/acounts");
   var httpClient = requestJson.createClient(urlBaseMlab);
   console.log("Cliente HTTP Mlab creado ");
   var id = req.params.user_id;
   var fil='f={_id:0}&q={user_id:' + id + '}&' ;
   httpClient.get('account?' + fil + urlApiKey,
    function(err, respuestaMlab, body){
      console.log("Error: " + err);
      console.log("Body: " + body);
      console.log("respuestaMlab: " + respuestaMlab);
      var respuesta = {};
      respuesta = !err ? body : {"msg" : "Error al recuperar usuarios en mlab"}
      res.send(respuesta);

    });
});

// Petición GET con parámetros (req.params) ACCOUNTS
app.get(URLbase + 'users/:user_id/accounts/:iban',
 function (req, res) {
   console.log("GET /colapi/v2/users/id/accounts/IBAM");
   var httpClient = requestJson.createClient(urlBaseMlab);
   console.log("Cliente HTTP Mlab creado ");
   var id = req.params.user_id;
   var iban = req.params.iban;
   var fil='f={"_id":0}&q={"IBAN":' + iban + '}&';
   httpClient.get('account?' + fil + urlApiKey,
    function(err, respuestaMlab, body){
      console.log("Error: " + err);
      console.log("Body: " + body);
      console.log("respuestaMlab: " + respuestaMlab);
      respuesta = !err ? body : {"msg" : "Error al recuperar usuarios de mlab"}
      if (body[0]==undefined){
        res.send(204);
      }else{

        var respuesta = body;
        res.send(respuesta);
      }

    });
});

//LOGIN Y LOGOUT
// LOGIN - users.json

app.post(URLbase + 'login',
  function(req, res) {
    console.log("POST apicol/V2/login");
  var user=req.body.email;
  var pass = req.body.password;
  //Peticion Get.
  var fil='f={_id:0}&q={"email" :'+ '{$eq :"' +user + '"}'+ ', "password" :'+'{$eq :"' + pass + '"}' + '}&';
 var clienteMlab = requestJson.createClient(urlBaseMlab + "user?" + fil + urlApiKey);
 clienteMlab.get('',
   function(err, respuestaMlab, bodyusers){

     if (bodyusers[0]==undefined){
       res.send({"msg" : "Usuario o Contraseñas Incorrectas"});
     }else{
       clienteMlab2 = requestJson.createClient(urlBaseMlab + "/user");
      var newJson = {};
       var logge = bodyusers[0];
       logge.logged="true"; //añade propiedad al users.json
      console.log(logge);
        //var cambio = '{"$set":' + JSON.stringify(logge) + '}'
        var cambio = '{"$set":' + JSON.stringify({"logged" : true}) + '}'
        console.log(cambio);
        clienteMlab2.put('?q={"email": "' + user + '"}&' + urlApiKey, JSON.parse(cambio),
        function(err, resM, body) {
          //respuesta = body.n !=0 ? {"msg" : "Actualizacion realizada"} : {"msg" : "Error usuario no existe"};
          respuesta = body.n !=0 ? bodyusers : {"msg" : "Error usuario no existe"};
            res.send(respuesta);
          });
     }
    });
});
  // LOGOUT

app.post(URLbase + 'logout',
function(req, res) {
  console.log("POST apicol/V2/login");
var user=req.body.email;
var pass = req.body.password;
//Peticion Get.
var fil='f={_id:0}&q={"email" :'+ '{$eq :"' +user + '"}' + '}&';
var clienteMlab = requestJson.createClient(urlBaseMlab + "user?" + fil + urlApiKey);
clienteMlab.get('',
 function(err, respuestaMlab, bodyusers){

   if (bodyusers[0]==undefined){
     res.send("Usuario o Contraseñas Incorrectas");
   }else{
     clienteMlab2 = requestJson.createClient(urlBaseMlab + "/user");
    var newJson = {};
     var logge = bodyusers[0];
     delete logge.logged;//añade propiedad al users.json
     //logge.logged="true"; //añade propiedad al users.json
    console.log(logge);
      clienteMlab2.put('?q={"email": "' + user + '"}&' + urlApiKey, JSON.parse(JSON.stringify(logge)),
      function(err, resM, body) {
        respuesta = body.n !=0 ? bodyusers : {"msg" : "Error en el Logout"};
          res.send(respuesta);
        });
   }
  });
});


  function writeUserDataToFile(data) {
    var fs =require('fs');
    var jsonUserData = JSON.stringify(data);

    fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) {//funcion mejorada para gestionar errores de escritura
      if(err){
        console.log(err);
      }else{
        console.log("Datos Escritos en 'users.json'.");
      }
    })
  }
