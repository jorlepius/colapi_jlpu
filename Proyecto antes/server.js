var express = require('express'); /*Import de libreria express*/
var bodyParser = require('body-parser')/*Import de libreria body-parser*/
var app = express();/*Asignacion de la libreria a una variable*/
var port = process.env.PORT || 3010;
var usersFile = require('./users.json');
var urlBase = "/colapi/V1/";

app.listen(port);

console.log("Colapi escuchando en puerto "+ port +"...");
app.use(bodyParser.json());//

//Peticion GET con Parametros (req.params)
app.get(urlBase + 'users/:id/:otro',
  function (req, res) {
    console.log("GET /COLAPI/V1/users/:id");
    console.log('req.params.otro : ' + req.params.otro);
    var pos = req.params.id;/*accede al parametri ID*/
    console.log("pos :" + pos);
      res.send(usersFile[pos - 1]);
      //res.sendfile(usersFile[pos - 1]); Obsoleta
      //var respuesta = req.params.id + ", " + req.params.otro;
      //res.send(req.params);
});


//Peticion GET con Query String (req.Query)

app.get (urlBase + 'users',
  function(peticion, respuesta){
    console.log("GET con Query string");
    console.log(peticion.query.id);
    respuesta.send({"msg" : "GET con Query String"});

  });

app.post(urlBase + 'users',
function (req, res) {
  console.log("POST /COLAPI/V1");
  console.log(req.body);
  console.log(req.body.first_name);
  //console.log(req.params);
  //console.log(req.headers);
  var newId =usersFile.length + 1;
  var jsonId={};
  jsonId= req.body;
  jsonId.id = newId;
  console.log(newId);
  usersFile.push(jsonId);
  res.send({"msg" : "Usuario Creado Correctamente.", jsonId});
  //res.sendFile('users.json', {root : __dirname}); //Remplazo del sendfile.
});

app.put(urlBase + 'users/:id',
function (req, res) {
  console.log("PUT /COLAPI/V1/USER/:id");
  var idBuscar = req.params.id;
  var updateUser = req.body;
  console.log(req.body);
  for (var i=0; i < usersFile.length; i++) {
    if(usersFile[i].id == idBuscar){
      console.log("Id Encontrado" + idBuscar);
      i=usersFile.length;
    }else{
      console.log("Usuario con id " + idBuscar + "No existe");
    }
  }
  res.send("PUT procesado");
});

app.delete(urlBase + 'users/:id',
function (req, res) {
  console.log("DELETE/COLAPI/V1/USER");
  var idDelete = req.params.id;
  var deleteUser = req.body;
  var encontrado = false;
  console.log(req.body);
  for (var i=0; (i < usersFile.length) && !encontrado; i++) {
    if(usersFile[i].id == idDelete){
      usersFile.splice(i,1);
      encontrado=true;
      res.send({
        "msg" : "Peticion DELETE correcta"
      });
    }
  }

  if(!encontrado){
    res.send({
      "msg" : "Peticion de Delete Id no existe"
    });
  }
});
