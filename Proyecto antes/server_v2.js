var express = require('express'); /*Import de libreria express*/
var bodyParser = require('body-parser');/*Import de libreria body-parser*/
var app = express();/*Asignacion de la libreria a una variable*/
var port = process.env.PORT || 3010;
var usersFile = require('./users.json');
var URLbase = "/colapi/V2/";

app.listen(port);

console.log("Colapi escuchando en puerto "+ port +"...");
app.use(bodyParser.json());//

// GET users
app.get(URLbase + 'users',
 function(request, response) {
   console.log(URLbase);
   console.log(usersFile);
   response.send(usersFile);
});

// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id/:otro',
 function (req, res) {
   console.log("GET /colapi/v2/users/:id/:otro");
   console.log(req.params);
   console.log('req.params.id: ' + req.params.id);
   console.log('req.params.otro: ' + req.params.otro);
   var pos = req.params.id;/*accede al parametri ID*/
   var otro = req.params.otro;/*accede al parametri ID*/
  // res.send(usersFile[pos - 1]);
   var respuesta = req.params;
   res.send(respuesta);
});

// Petición GET con Query String (req.query)
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET con query string.");
   console.log(req.query.id);
   console.log(req.query.country);
   res.send(usersFile[pos - 1]);
   respuesta.send({"msg" : "GET con query string"});
});

app.post(URLbase + 'users',
function (req, res) {
  console.log("POST /COLAPI/V1");
  console.log(req.body);
  console.log(req.body.first_name);
  //console.log(req.params);
  //console.log(req.headers);
  var newId =usersFile.length + 1;
  var jsonId={};
  jsonId= req.body;
  jsonId.id = newId;
  console.log(newId);
  usersFile.push(jsonId);
  res.send({"msg" : "Usuario Creado Correctamente.", jsonId});
  //res.sendFile('users.json', {root : __dirname}); //Remplazo del sendfile.
});

app.put(URLbase + 'users/:id',
function (req, res) {
  console.log("PUT /COLAPI/V1/USER/:id");
  var idBuscar = req.params.id;
  var updateUser = req.body;
  console.log(req.body);
  for (var i=0; i < usersFile.length; i++) {
    if(usersFile[i].id == idBuscar){
      console.log("Id Encontrado" + idBuscar);
      i=usersFile.length;
    }else{
      console.log("Usuario con id " + idBuscar + "No existe");
    }
  }
  res.send("PUT procesado");
});

app.delete(URLbase + 'users/:id',
function (req, res) {
  console.log("DELETE/COLAPI/V1/USER");
  var idDelete = req.params.id;
  var deleteUser = req.body;
  var encontrado = false;
  console.log(req.body);
  for (var i=0; (i < usersFile.length) && !encontrado; i++) {
    if(usersFile[i].id == idDelete){
      usersFile.splice(i,1);
      encontrado=true;
      res.send({
        "msg" : "Peticion DELETE correcta"
      });
    }
  }

  if(!encontrado){
    res.send({
      "msg" : "Peticion de Delete Id no existe"
    });
  }
});
// LOGIN - users.json
app.post(URLbase + 'login',
  function(req, res) {
    console.log("POST apicol/V2/login");
  var user=req.body.email;
  var pass = req.body.password;
  console.log(req.body.email);
  console.log(req.body.password);
  for(us of usersFile){
    if(us.email == user){
      if(us.password == pass){
        us.logged =true; //añade propiedad al users.json
        writeUserDataToFile(usersFile);
        res.send({"msg:" : "Login correcto", "Id" : us.id, "Nombre" : us.first_name, "Apellido" : us.last_name});
      }else{
        res.send({"msg" : "Usuario o contraseña incorrecta"});
      }
    }
  }
  res.send({"msg" : "Usuario o contraseña incorrecta"});
});
  // LOGOUT

app.post(URLbase + 'logout',
  function(req, res) {
    console.log("POST apicol/V2/login")
    var user=req.body.email;
    var pass = req.body.password;

    for(us of usersFile){
      if(us.email == user){
        if(us.password == pass){
          delete us.logged;//añade propiedad al users.json
          writeUserDataToFile(usersFile);
          res.send({"msg:" : "Sesion terminada Correctamente"});
        }else{
          res.send({"msg" : "Logout Incorrecto"});
        }
      }
    }
    res.send({"msg" : "Logout Incorrecto"});
});


  function writeUserDataToFile(data) {
    var fs =require('fs');
    var jsonUserData = JSON.stringify(data);

    fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) {//funcion mejorada para gestionar errores de escritura
      if(err){
        console.log(err);
      }else{
        console.log("Datos Escritos en 'users.json'.");
      }
    })
  }
